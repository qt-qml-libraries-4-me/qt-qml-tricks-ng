
#include "QQmlIntrospector.h"

QQuickGroupItem::QQuickGroupItem (QQuickItem * parent)
    : QQuickItem { parent }
    , m_title    { }
    , m_icon     { Q_NULLPTR }
{ }

QQuickGroupItem::~QQuickGroupItem (void) { }

QQmlIntrospector::QQmlIntrospector (QObject * parent) : QObject (parent) { }

QObject * QQmlIntrospector::qmlSingletonProvider (QQmlEngine * qmlEngine, QJSEngine * jsEngine) {
    Q_UNUSED (qmlEngine)
    Q_UNUSED (jsEngine)
    return new QQmlIntrospector;
}

bool QQmlIntrospector::isGroupItem (QObject * object) {
    return (qobject_cast<QQuickGroupItem *> (object) != Q_NULLPTR);
}

QQuickWindow * QQmlIntrospector::window (QQuickItem * item) {
    return (item != Q_NULLPTR ? item->window () : Q_NULLPTR);
}
