import qbs;

Product {
    name: "lib_qt_qml_tricks_ng";
    type: "staticlibrary";
    cpp.cxxLanguageVersion: "c++11";
    cpp.includePaths: [
        ".",
        "./core",
        "./core/macros",
        "./core/models",
        "./core/helpers",
        "./gui",
        "./gui/containers",
        "./gui/helpers",
        "./gui/shapes",
    ]

    readonly property stringList qmlImportsPath : "./imports";

    Export {
        cpp.cxxLanguageVersion: "c++11";
        cpp.includePaths: [
            ".",
            "./core",
            "./core/macros",
            "./core/models",
            "./core/helpers",
            "./gui",
            "./gui/containers",
            "./gui/helpers",
            "./gui/shapes",
        ]

        readonly property stringList qmlImportsPath : "./imports";

        Depends {
            name: "Qt";
            submodules: ["core", "gui", "qml", "quick", "svg"];
        }
        Depends {
            name: "cpp";
        }
    }
    Depends {
        name: "Qt";
        submodules: ["core", "gui", "qml", "quick", "svg"];
    }
    Depends {
        name: "cpp";
    }
    Group {
        name: "C++";
        files: [
            "QtQmlTricks.cpp",
            "QtQmlTricks.h",
            "core/helpers/QQmlFsSingleton.cpp",
            "core/helpers/QQmlFsSingleton.h",
            "core/helpers/QQmlIntrospector.cpp",
            "core/helpers/QQmlIntrospector.h",
            "core/helpers/QQmlMimeIconsHelper.cpp",
            "core/helpers/QQmlMimeIconsHelper.h",
            "core/macros/QmlEnumHelpers.h",
            "core/macros/QmlPropertyHelpers.h",
            "core/models/QQmlFastObjectListModel.h",
            "core/models/QQmlObjectListModel.h",
            "core/models/QQmlVariantListModel.h",
            "gui/containers/QQmlContainerEnums.h",
            "gui/containers/QQuickAbstractContainerBase.cpp",
            "gui/containers/QQuickAbstractContainerBase.h",
            "gui/containers/QQuickColumnContainer.cpp",
            "gui/containers/QQuickColumnContainer.h",
            "gui/containers/QQuickContainerAttachedObject.cpp",
            "gui/containers/QQuickContainerAttachedObject.h",
            "gui/containers/QQuickFastObjectListView.cpp",
            "gui/containers/QQuickFastObjectListView.h",
            "gui/containers/QQuickFormContainer.cpp",
            "gui/containers/QQuickFormContainer.h",
            "gui/containers/QQuickGridContainer.cpp",
            "gui/containers/QQuickGridContainer.h",
            "gui/containers/QQuickRowContainer.cpp",
            "gui/containers/QQuickRowContainer.h",
            "gui/containers/QQuickStackContainer.cpp",
            "gui/containers/QQuickStackContainer.h",
            "gui/containers/QQuickTableContainer.cpp",
            "gui/containers/QQuickTableContainer.h",
            "gui/helpers/QQuickExtraAnchors.cpp",
            "gui/helpers/QQuickExtraAnchors.h",
            "gui/helpers/QQuickSvgIconHelper.cpp",
            "gui/helpers/QQuickSvgIconHelper.h",
            "gui/helpers/QQuickWindowIconHelper.cpp",
            "gui/helpers/QQuickWindowIconHelper.h",
            "gui/shapes/QQuickRoundedRectangleItem.cpp",
            "gui/shapes/QQuickRoundedRectangleItem.h",
        ]
    }
    Group {
        name: "Resource Files";
        fileTags: "qt.core.resource_data";
        files: [
            "qtqmltricks_svgicons_actions.qrc",
            "qtqmltricks_svgicons_devices.qrc",
            "qtqmltricks_svgicons_filetypes.qrc",
            "qtqmltricks_svgicons_others.qrc",
            "qtqmltricks_svgicons_services.qrc",
            "qtqmltricks_uielements.qrc",
        ]
    }
    Group {
        name: "Markdown";
        files: [
            "LICENSE.md",
            "README.md",
        ]
    }
}
