#ifndef QTQMLTRICKS_H
#define QTQMLTRICKS_H

#include <QQmlEngine>

class QtQmlTricks {
public:
    static void registerComponents (QQmlEngine * engine = Q_NULLPTR);
};

#endif // QTQMLTRICKS_H
