
#include "QtQmlTricks.h"

#include "QQmlContainerEnums.h"
#include "QQmlFastObjectListModel.h"
#include "QQmlObjectListModel.h"
#include "QQuickColumnContainer.h"
#include "QQuickContainerAttachedObject.h"
#include "QQuickExtraAnchors.h"
#include "QQuickFastObjectListView.h"
#include "QQuickFormContainer.h"
#include "QQuickGridContainer.h"
#include "QQuickRowContainer.h"
#include "QQuickStackContainer.h"
#include "QQuickTableContainer.h"
#include "QQuickSvgIconHelper.h"
#include "QQuickRoundedRectangleItem.h"
#include "QQmlMimeIconsHelper.h"
#include "QQuickWindowIconHelper.h"
#include "QQmlIntrospector.h"
#include "QQmlFsSingleton.h"
#include "QQmlVariantListModel.h"

#include <qqml.h>

void QtQmlTricks::registerComponents (QQmlEngine * engine) {
    static const QString ERR_ENUM_CLASS    { QStringLiteral ("Enum-class !") };
    static const QString ERR_ATTACHED_OBJ  { QStringLiteral ("Attached-object class !") };
    static const QString ERR_ABSTRACT_BASE { QStringLiteral ("Abstract base class !") };
    qmlRegisterType<QQuickColumnContainer>                    ("QtQmlTricks", 3, 0, "ColumnContainer");
    qmlRegisterType<QQuickGridContainer>                      ("QtQmlTricks", 3, 0, "GridContainer");
    qmlRegisterType<QQuickStackContainer>                     ("QtQmlTricks", 3, 0, "StackContainer");
    qmlRegisterType<QQuickFormContainer>                      ("QtQmlTricks", 3, 0, "FormContainer");
    qmlRegisterType<QQuickRowContainer>                       ("QtQmlTricks", 3, 0, "RowContainer");
    qmlRegisterType<QQuickTableContainer>                     ("QtQmlTricks", 3, 0, "TableContainer");
    qmlRegisterType<QQmlTableDivision>                        ("QtQmlTricks", 3, 0, "TableDiv");
    qmlRegisterUncreatableType<QQmlTableCellPosH>             ("QtQmlTricks", 3, 0, "TableCellPosH",            ERR_ENUM_CLASS);
    qmlRegisterUncreatableType<QQmlTableCellPosV>             ("QtQmlTricks", 3, 0, "TableCellPosV",            ERR_ENUM_CLASS);
    qmlRegisterUncreatableType<QQmlTableAttachedObject>       ("QtQmlTricks", 3, 0, "TableCell",                ERR_ATTACHED_OBJ);
    qmlRegisterUncreatableType<VerticalDirections>            ("QtQmlTricks", 3, 0, "VerticalDirections",       ERR_ENUM_CLASS);
    qmlRegisterUncreatableType<HorizontalDirections>          ("QtQmlTricks", 3, 0, "HorizontalDirections",     ERR_ENUM_CLASS);
    qmlRegisterUncreatableType<FlowDirections>                ("QtQmlTricks", 3, 0, "FlowDirections",           ERR_ENUM_CLASS);
    qmlRegisterUncreatableType<QQuickExtraAnchors>            ("QtQmlTricks", 3, 0, "ExtraAnchors",             ERR_ATTACHED_OBJ);
    qmlRegisterUncreatableType<QQuickContainerAttachedObject> ("QtQmlTricks", 3, 0, "Container",                ERR_ATTACHED_OBJ);
    qmlRegisterUncreatableType<QQmlObjectListModelBase>       ("QtQmlTricks", 3, 0, "ObjectListModel",          ERR_ABSTRACT_BASE);
    qmlRegisterUncreatableType<QQmlVariantListModelBase>      ("QtQmlTricks", 3, 0, "VariantListModel",         ERR_ABSTRACT_BASE);
    qmlRegisterUncreatableType<QQmlFastObjectListModelBase>   ("QtQmlTricks", 3, 0, "FastObjectListModel",      ERR_ABSTRACT_BASE);
    qmlRegisterUncreatableType<QQmlAbstractMaterial>          ("QtQmlTricks", 3, 0, "AbstractMaterial",         ERR_ABSTRACT_BASE);
    qmlRegisterUncreatableType<QQmlAbstractGradientMaterial>  ("QtQmlTricks", 3, 0, "AbstractGradientMaterial", ERR_ABSTRACT_BASE);
    qmlRegisterType<QQuickFastObjectListView>                 ("QtQmlTricks", 3, 0, "FastObjectListView");
    qmlRegisterType<QQmlFlatColorMaterial>                    ("QtQmlTricks", 3, 0, "FlatColorMaterial");
    qmlRegisterType<QQmlVerticalGradientMaterial>             ("QtQmlTricks", 3, 0, "VerticalGradientMaterial");
    qmlRegisterType<QQmlHorizontalGradientMaterial>           ("QtQmlTricks", 3, 0, "HorizontalGradientMaterial");
    qmlRegisterType<QQuickRoundedRectangleItem>               ("QtQmlTricks", 3, 0, "RoundedRectangle");
    qmlRegisterType<QQuickSvgIconHelper>                      ("QtQmlTricks", 3, 0, "SvgIconHelper");
    qmlRegisterType<QQmlMimeIconsHelper>                      ("QtQmlTricks", 3, 0, "MimeIconsHelper");
    qmlRegisterType<QQuickGroupItem>                          ("QtQmlTricks", 3, 0, "Group");
    qmlRegisterType<QQuickWindowIconHelper>                   ("QtQmlTricks", 3, 0, "WindowIconHelper");
    qmlRegisterType<QQmlFileSystemModelEntry>                 ("QtQmlTricks", 3, 0, "FileSystemModelEntry");
    qmlRegisterSingletonType<QQmlFileSystemSingleton>         ("QtQmlTricks", 3, 0, "FileSystem",   &QQmlFileSystemSingleton::qmlSingletonProvider);
    qmlRegisterSingletonType<QQmlIntrospector>                ("QtQmlTricks", 3, 0, "Introspector", &QQmlIntrospector::qmlSingletonProvider);
    QQuickSvgIconHelper::setBasePath (":/QtQmlTricks/icons");
    if (engine != Q_NULLPTR) {
        engine->addImportPath ("qrc:///imports");
    }
    else {
        qWarning () << "You didn't pass a QML engine to the register function,"
                    << "some features (mostly plain QML components, and icon theme provider) won't work !";
    }
}
