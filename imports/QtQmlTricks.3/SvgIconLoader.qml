import QtQuick 2.6;
import QtQmlTricks 3.0;

Item {
    id: base;
    visible: (icon !== "");
    implicitWidth: helper.size;
    implicitHeight: helper.size;

    property int   size   : Style.iconSize (1);
    property bool  dimmed : !base.enabled;
    property color color  : Style.colorNone;
    property alias icon   : helper.icon;

    Image {
        id: img;
        cache: true;
        smooth: false;
        opacity: (dimmed ? 0.65 : 1.0);
        fillMode: Image.Pad;
        antialiasing: false;
        asynchronous: true;
        anchors.centerIn: parent;
        anchors.alignWhenCentered: true;

        SvgIconHelper on source {
            id: helper;
            size: base.size;
            color: (base.dimmed ? Style.colorBorder : base.color);
        }
    }
}
