import QtQuick 2.6;
import QtQmlTricks 3.0;

Rectangle {
    id: toolbar;
    height: (layout.height + layout.anchors.margins * 2);
    gradient: Style.gradientIdle (Style.colorWindow);
    ExtraAnchors.topDock: parent;

    property int padding : Style.spacingNormal;

    default property alias content : layout.data;

    Line { ExtraAnchors.bottomDock: parent; }
    RowContainer {
        id: layout;
        spacing: toolbar.padding;
        anchors.margins: toolbar.padding;
        anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        ExtraAnchors.horizontalFill: parent;

        // NOTE : CONTENT GOES HERE
    }
}
