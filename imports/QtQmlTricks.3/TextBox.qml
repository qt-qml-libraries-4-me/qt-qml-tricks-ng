import QtQuick 2.6;
import QtQmlTricks 3.0;

FocusScope {
    id: base;
    implicitWidth: Math.ceil (input.implicitWidth + padding * 2);
    implicitHeight: Math.ceil (Math.max (input.implicitHeight, holder.implicitHeight) + padding * 2);

    property int   padding    : Style.spacingNormal;
    property bool  hasClear   : false;
    property bool  isPassword : false;
    property bool  emphasis   : false;
    property color backColor  : Style.colorEditable;
    property color textColor  : Style.colorForeground;
    property alias text       : input.text;
    property alias readOnly   : input.readOnly;
    property alias textFont   : input.font;
    property alias textAlign  : input.horizontalAlignment;
    property alias textHolder : holder.text;
    property alias inputMask  : input.inputMask;
    property alias validator  : input.validator;
    property alias acceptable : input.acceptableInput;
    property alias rounding   : rect.radius;
    property alias value      : input.text;

    readonly property bool isEmpty : (text.trim () === "");

    signal accepted ();

    function selectAll () {
        input.selectAll ();
    }

    function clear () {
        input.text = "";
    }

    Rectangle {
        id: rect;
        radius: Style.roundness;
        enabled: base.enabled;
        visible: !readOnly;
        antialiasing: radius;
        gradient: (enabled ? Style.gradientEditable (backColor) : Style.gradientDisabled ());
        border {
            width: (input.activeFocus && !readOnly ? Style.lineSize * 2 : Style.lineSize);
            color: (input.activeFocus ? Style.colorSelection : Style.colorBorder);
        }
        anchors.fill: parent;
    }
    Item {
        clip: (input.contentWidth > input.width);
        enabled: base.enabled;
        anchors {
            fill: parent;
            margins: rect.border.width;
        }

        TextInput {
            id: input;
            focus: true;
            color: (enabled ? textColor : Style.colorBorder);
            enabled: base.enabled;
            selectByMouse: true;
            selectionColor: Style.colorSelection;
            selectedTextColor: Style.colorEditable;
            activeFocusOnPress: true;
            echoMode: (isPassword ? TextInput.Password : TextInput.Normal);
            font {
                family: Style.fontName;
                weight: (emphasis ? Font.Bold : (Style.useSlimFonts ? Font.Light : Font.Normal));
                pixelSize: Style.fontSizeNormal;
            }
            anchors {
                margins: base.padding;
                verticalCenter: (parent ? parent.verticalCenter : undefined);
            }
            ExtraAnchors.horizontalFill: parent;
            onAccepted: { base.accepted (); }
        }
        MouseArea {
            enabled: base.enabled;
            visible: (input.text !== "" && hasClear);
            implicitWidth: height;
            ExtraAnchors.rightDock: parent;
            onClicked: {
                base.focus = false;
                clear ();
            }

            Rectangle {
                rotation: -90;
                implicitWidth: (parent.width)
                implicitHeight: (parent.height * 2);
                gradient: Gradient {
                    GradientStop { position: 0.0; color: Style.colorNone;  }
                    GradientStop { position: 0.5; color: backColor; }
                    GradientStop { position: 1.0; color: backColor; }
                }
                anchors {
                    verticalCenter: (parent ? parent.verticalCenter : undefined);
                    horizontalCenter: (parent ? parent.left : undefined);
                }
            }
            SymbolLoader {
                id: cross;
                size: Style.fontSizeNormal;
                color: (enabled ? Style.colorForeground : Style.colorBorder);
                symbol: Style.symbolCross;
                enabled: base.enabled;
                anchors.centerIn: parent;
            }
        }
    }
    TextLabel {
        id: holder;
        color: Style.opacify (Style.colorBorder, 0.85);
        enabled: base.enabled;
        visible: (!input.activeFocus && input.text.trim ().length === 0 && !readOnly);
        horizontalAlignment: input.horizontalAlignment;
        font {
            weight: Font.Normal;
            family: input.font.family;
            pixelSize: input.font.pixelSize;
        }
        anchors {
            margins: base.padding;
            verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        ExtraAnchors.horizontalFill: parent;
    }
}
