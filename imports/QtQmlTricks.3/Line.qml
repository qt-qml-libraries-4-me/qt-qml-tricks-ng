import QtQuick 2.6;
import QtQmlTricks 3.0;

Rectangle {
    color: Style.colorBorder;
    implicitWidth: Style.lineSize;
    implicitHeight: Style.lineSize;
}
