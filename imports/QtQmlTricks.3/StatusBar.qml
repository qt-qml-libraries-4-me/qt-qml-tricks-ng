import QtQuick 2.6;
import QtQmlTricks 3.0;

Rectangle {
    id: statusbar;
    height: (layout.height + layout.anchors.margins * 2);
    gradient: Style.gradientIdle (Style.colorWindow);
    ExtraAnchors.bottomDock: parent;

    default property alias content : layout.data;

    Line { ExtraAnchors.topDock: parent; }
    RowContainer {
        id: layout;
        spacing: Style.spacingNormal;
        anchors.margins: Style.spacingNormal;
        anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        ExtraAnchors.horizontalFill: parent;

        // NOTE : CONTENT GOES HERE
    }
}
